let FAClient = null;
let recordId = null;

const SERVICE = {
  name: 'FreeAgentService',
  appletId: `aHR0cHM6Ly9pdmFuY2FycmlsbG8uZ2l0bGFiLmlvL3Rvd25oYWxsdGVzdC8=`,
};

function startupService() {

  console.log("Excecuted");

  FAClient = new FAAppletClient({
    appletId: SERVICE.appletId,
  });

  FAClient.on("openForm",openForm);
  
}

async function openForm(data) {
    let iFrame = document.createElement('iframe');
    iFrame.src = data.formURL;
    iFrame.style = 'width: 100%; height: 100%; border: none;';
    removeTextMessage();
    divFrame = document.getElementById('iFrame');
    while(divFrame.firstChild){
      divFrame.removeChild(divFrame.firstChild);
    }
    divFrame.appendChild(iFrame);
}

function removeTextMessage() {
  try{
    document.getElementById('loading-text').remove();
  }catch(e){
    console.log("Used Again");
  }
}